<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\Staff;
use App\Photo;
use App\Product;

Route::get('/', function () {
    return view('welcome');
});


Route::get('/create',function(){
    $staf= Staff::findOrFail(1);
    $staf->photos()->create(['path'=>'another.jpg']);
});

Route::get('/create/product', function(){
    $product= Product::findOrFail(1);
    $product->photos()->create(['path'=>'bucket.jpg']);
});

Route::get('read/product', function(){
    $products = Product::findOrFail(1);

    foreach($products->photos as $product){
        echo $product;
    }
});

Route::get('update',function(){
    $staff = Staff::findOrFail(1);

    $photo = $staff->photos()->whereId(1)->first();

    $photo->path ="update.jpg";

    $photo->save();
});

Route::get('delete',function(){
    $staff= Staff::findOrFail(1);

    $staff->photos()->whereId(1)->delete();
});

Route::get('assign',function(){
    $staff = Staff::findOrFail(3);
    $product =Product::findOrFail(1);
    $photo = Photo::findOrFail(2);

    $staff->photos()->save($photo);
});